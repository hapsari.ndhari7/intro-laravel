<!DOCTYPE html>

<head>
    <title>Sign Up</title>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <p><label>First name:</label></p>
        <p><input type="text" name="first_name"></p>

        <p><label>Last name:</label></p>
        <p><input type="text" name="last_name"></p>

        <p><label>Gender:</label></p>
        <p>
            <label><input type="radio" name="gender" value="male">Male</label><br/>
            <label><input type="radio" name="gender" value="female">Female</label><br/>
            <label><input type="radio" name="gender" value="other">Other</label>
        </p>

        <p><label>Nationality:</label></p>
        <p>
            <select name="nationality">
                <option value="indonesian">Indonesian</option>
                <option value="singaporean">Singaporean</option>
                <option value="malaysian">Malaysian</option>
                <option value="australian">Australian</option>
            </select>
        </p>

        <p><label>Language Spoken:</label></p>
        <p>
            <label><input type="checkbox" name="language" value="bahasa">Bahasa Indonesia</label><br/>
            <label><input type="checkbox" name="language" value="english">English</label><br/>
            <label><input type="checkbox" name="language" value="other">Other</label>
        </p>

        <p><label>Bio:</label></p>
        <p>
            <textarea name="bio" cols="30" rows="10"></textarea><br/>
            <input type="submit" name="submit" value="Sign Up">
        </p>
    </form>
</head>
<body>
    
</body>
</html>